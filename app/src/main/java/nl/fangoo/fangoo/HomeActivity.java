package nl.fangoo.fangoo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

import nl.fangoo.fangoo.customcontrol.FangooTextView;
import nl.fangoo.fangoo.presenter.HomeActivityPresenter;
import nl.fangoo.fangoo.presenter.HomeActivityPresenterImpl;
import nl.fangoo.fangoo.utils.Initializer;
import nl.fangoo.fangoo.view.HomeActivityView;

public class HomeActivity extends AppCompatActivity implements HomeActivityView {

    private HomeActivityPresenter homeActivityPresenter;
    private Context context;

    private Toolbar toolbar;
    private FangooTextView tvAppbarTitle;
    private ImageButton ivLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        context = this;

        homeActivityPresenter = new HomeActivityPresenterImpl(context, this);
        homeActivityPresenter.initializePresenter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void initUI() {
        toolbar = Initializer.initialize(toolbar, R.id.ab_home, this);
        tvAppbarTitle = Initializer.initialize(R.id.tv_appbar_title,toolbar);
        ivLogin = (ImageButton) findViewById(R.id.iv_login);
    }

    @Override
    public void initActionbar() {
        setSupportActionBar(toolbar);
        tvAppbarTitle.setText("Home");
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    @Override
    public void setListener() {
        ivLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
