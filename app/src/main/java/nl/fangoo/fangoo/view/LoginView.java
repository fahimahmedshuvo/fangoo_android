package nl.fangoo.fangoo.view;

import nl.fangoo.fangoo.listeners.CommonViewCallbacks;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public interface LoginView extends CommonActivityView, CommonViewCallbacks {
    void goToDashboard();
}
