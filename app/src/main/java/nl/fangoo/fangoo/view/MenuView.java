package nl.fangoo.fangoo.view;

import java.util.ArrayList;

import nl.fangoo.fangoo.listeners.CommonViewCallbacks;
import nl.fangoo.fangoo.model.DashboardItem;

/**
 * Created by Fahim Ahmed on 12-12-16.
 */

public interface MenuView extends CommonActivityView, CommonViewCallbacks{
    void goToHomeScreen();

    void setDashboard(ArrayList<DashboardItem> dashboardItems);

}
