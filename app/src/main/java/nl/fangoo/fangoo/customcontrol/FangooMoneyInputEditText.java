package nl.fangoo.fangoo.customcontrol;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.View;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 10-November-2016.
 */
public class FangooMoneyInputEditText extends TextInputEditText {
    private static String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";
    private Context context;

    public FangooMoneyInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
        setOnFocusChangeListener(onFocusChangeListener);
    }

    public FangooMoneyInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
        setOnFocusChangeListener(onFocusChangeListener);
    }

    public String getMoney() {
        String text = getText().toString();
        if (text.length() > 2) {
            return text.substring(2, text.length());
        } else {
            return text;
        }
    }

    View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            String text = getText().toString();
            String sign = context.getString(R.string.euro_sign) + " ";
            if (hasFocus && !text.startsWith(sign)) {
                setText(sign + text);
                setSelection(getText().length());
            } else if (!hasFocus && text.length() <= 2) {
                setText("");
            }
        }
    };
}
