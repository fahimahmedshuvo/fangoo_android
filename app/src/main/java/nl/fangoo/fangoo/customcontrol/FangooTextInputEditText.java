package nl.fangoo.fangoo.customcontrol;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.utils.ApplicationUtils;


/**
 * Created by Zahidul_Islam_George on 10-November-2016.
 */
public class FangooTextInputEditText extends TextInputEditText {
    private static String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";
    public FangooTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
    }

    public FangooTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
    }
}
