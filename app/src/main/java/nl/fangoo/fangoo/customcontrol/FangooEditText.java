package nl.fangoo.fangoo.customcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class FangooEditText extends EditText {
    private static String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";
    public FangooEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
    }

    public FangooEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FangooEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
        ApplicationUtils.setHintTextColor(this, context, R.color.gray);
    }


}
