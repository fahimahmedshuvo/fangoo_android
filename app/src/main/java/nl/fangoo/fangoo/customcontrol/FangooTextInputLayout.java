package nl.fangoo.fangoo.customcontrol;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

import nl.fangoo.fangoo.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 10-November-2016.
 */
public class FangooTextInputLayout extends TextInputLayout {
    private static String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";
    public FangooTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }

    public FangooTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }
}
