package nl.fangoo.fangoo.customcontrol;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nl.fangoo.fangoo.utils.ApplicationUtils;

/**
 * Created by fam on 11/13/2016.
 */
public class FangooTabLayout extends TabLayout{

    private static String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";

    public FangooTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }

    public FangooTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }

    @Override
    public void addTab(Tab tab) {
        super.addTab(tab);

        Typeface mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");

        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();
        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
            if (tabViewChild instanceof TextView) {
                ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
            }
        }
    }
}
