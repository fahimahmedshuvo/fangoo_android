package nl.fangoo.fangoo.customcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import nl.fangoo.fangoo.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class FangooTextView extends TextView {
    private static String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";

    public FangooTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }

    public FangooTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }

    public void setTextBold(Context context){
        setTypeface(ApplicationUtils.getTypeface(Typeface.BOLD, context));
    }

    public void setTextNormal(Context context){
        setTypeface(ApplicationUtils.getTypeface(Typeface.NORMAL, context));
    }

    public void setTextItalic(Context context){
        setTypeface(ApplicationUtils.getTypeface(Typeface.ITALIC, context));
    }

    public void setTextBold(Context context, String text){
        setTypeface(ApplicationUtils.getTypeface(Typeface.BOLD, context));
        setText(text);
    }

    public void setTextNormal(Context context, String text){
        setTypeface(ApplicationUtils.getTypeface(Typeface.NORMAL, context));
        setText(text);
    }

    public void setTextItalic(Context context, String text){
        setTypeface(ApplicationUtils.getTypeface(Typeface.ITALIC, context));
        setText(text);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FangooTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        ApplicationUtils.getTypeface(textStyle, context);
    }
}
