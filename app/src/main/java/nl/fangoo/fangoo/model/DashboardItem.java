package nl.fangoo.fangoo.model;

/**
 * Created by Fahim Ahmed on 14-12-16.
 */

public class DashboardItem {
    private String itemName;

    public DashboardItem(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}

