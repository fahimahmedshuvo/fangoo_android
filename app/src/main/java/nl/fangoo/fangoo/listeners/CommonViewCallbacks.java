package nl.fangoo.fangoo.listeners;

/**
 * Created by Zahidul_Islam_George on 09-November-2016.
 */
public interface CommonViewCallbacks {
    void showProgress(String message);

    void hideProgress();

    void showMessage(String message);
}
