package nl.fangoo.fangoo.base;

import android.app.Application;

import nl.fangoo.fangoo.utils.TinyDB;


/**
 * Created by Fahim Ahmed on 11-11-16.
 */
public class FangooApplication extends Application {

    public static final String TAG = FangooApplication.class.getSimpleName();

    private static FangooApplication fangooApplication;

    @Override
    public void onCreate() {
        super.onCreate();

        fangooApplication = this;
    }

    public static FangooApplication getApplication(){
        return fangooApplication;
    }

    public TinyDB getTinyDB(){
        return new TinyDB(getApplicationContext());
    }

}
