package nl.fangoo.fangoo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import nl.fangoo.fangoo.customcontrol.FangooTextView;
import nl.fangoo.fangoo.model.DashboardItem;
import nl.fangoo.fangoo.presenter.DashboardPresenter;
import nl.fangoo.fangoo.presenter.DashboardPresenterImpl;
import nl.fangoo.fangoo.utils.Initializer;
import nl.fangoo.fangoo.view.MenuView;

/**
 * Created by Fahim Ahmed on 12-12-16.
 */

public class MenuActivity extends AppCompatActivity implements MenuView{

    private Context context;
    private Toolbar toolbar;
    private FangooTextView tvAppbarTitle;
    private DashboardPresenter dashboardPresenter;
    private DashboardAdapter dashboardAdapter;

    private ProgressDialog progressDialog;
    private RecyclerView rvDashboard;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        context = this;
        dashboardPresenter = new DashboardPresenterImpl(context, this);
        dashboardPresenter.initializePresenter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void setDashboard(ArrayList<DashboardItem> dashboardItems) {
        dashboardAdapter = new DashboardAdapter(dashboardItems, context, dashboardPresenter);
        rvDashboard.setAdapter(dashboardAdapter);
    }

    @Override
    public void goToHomeScreen() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_log_off:
                dashboardPresenter.logOut();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initUI() {
        toolbar = Initializer.initialize(toolbar, R.id.toolbar, this);
        tvAppbarTitle = Initializer.initialize(R.id.tv_appbar_title,toolbar);
        rvDashboard = Initializer.initialize(R.id.rvDashboard, MenuActivity.this, false, 1);
    }

    @Override
    public void initActionbar() {
        setSupportActionBar(toolbar);
        tvAppbarTitle.setText(getString(R.string.appbar_text_menu));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setListener() {

    }

    @Override
    public void showProgress(String message) {
        progressDialog = ProgressDialog.show(this, null,
                message, true);
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
    }
}
