package nl.fangoo.fangoo.presenter;

import android.content.Context;

import nl.fangoo.fangoo.view.HomeActivityView;

/**
 * Created by Fahim Ahmed on 11-12-16.
 */

public class HomeActivityPresenterImpl implements HomeActivityPresenter{

    private Context context;
    private HomeActivityView homeActivityView;

    public HomeActivityPresenterImpl(Context context, HomeActivityView homeActivityView){
        this.context = context;
        this.homeActivityView = homeActivityView;
    }

    @Override
    public void initializePresenter() {
        homeActivityView.initUI();
        homeActivityView.initActionbar();
        homeActivityView.setListener();
    }
}
