package nl.fangoo.fangoo.presenter;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public interface CommonActivityPresenter {
    void initializePresenter();
}
