package nl.fangoo.fangoo.presenter;

/**
 * Created by Fahim Ahmed on 12-12-16.
 */

public interface LoginPresenter extends CommonActivityPresenter{
    void loginUser(String email , String password);
}
