package nl.fangoo.fangoo.presenter;

import nl.fangoo.fangoo.model.DashboardItem;

/**
 * Created by Fahim Ahmed on 13-12-16.
 */

public interface DashboardPresenter extends CommonActivityPresenter{
    void logOut();

    void onDashboardItemSelected(DashboardItem dashboardItem);
}
