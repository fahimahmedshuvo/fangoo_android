package nl.fangoo.fangoo.presenter;

import android.content.Context;
import android.widget.Toast;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.data.server.APIInteractor;
import nl.fangoo.fangoo.data.server.APIInteractorImpl;
import nl.fangoo.fangoo.model.User;
import nl.fangoo.fangoo.utils.UserManager;
import nl.fangoo.fangoo.view.LoginView;

/**
 * Created by Fahim Ahmed on 12-12-16.
 */

public class LoginPresenterImpl implements LoginPresenter, APIInteractor.LoginListener{

    private Context context;
    private LoginView loginView;
    private UserManager userManager;
    private APIInteractor apiInteractor;

    public LoginPresenterImpl(Context context, LoginView loginView) {
        this.context = context;
        this.loginView = loginView;
        userManager = new UserManager(context);
        apiInteractor = new APIInteractorImpl(context);
    }

    @Override
    public void initializePresenter() {
        if (userManager.isLoggedIn()) {
            loginView.goToDashboard();
        } else {
            loginView.initUI();
            loginView.initActionbar();
            loginView.setListener();
        }
    }

    @Override
    public void loginUser(String email, String password) {
        loginView.showProgress(context.getString(R.string.logging_in_progress_message));
        apiInteractor.userLogin(this, email, password);
    }

    @Override
    public void onLoginSuccess(User user) {
        loginView.hideProgress();
        userManager.saveUserSession(user);
        loginView.goToDashboard();
    }

    @Override
    public void onFailed(String message) {
        loginView.hideProgress();
        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
    }
}
