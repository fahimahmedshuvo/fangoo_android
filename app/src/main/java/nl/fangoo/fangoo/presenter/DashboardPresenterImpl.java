package nl.fangoo.fangoo.presenter;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.data.server.APIInteractor;
import nl.fangoo.fangoo.data.server.APIInteractorImpl;
import nl.fangoo.fangoo.model.DashboardItem;
import nl.fangoo.fangoo.utils.ApplicationUtils;
import nl.fangoo.fangoo.utils.DashboardUtils;
import nl.fangoo.fangoo.utils.UserManager;
import nl.fangoo.fangoo.view.MenuView;

/**
 * Created by Fahim Ahmed on 13-12-16.
 */

public class DashboardPresenterImpl implements DashboardPresenter, APIInteractor.LogOutListener {

    private Context context;
    private UserManager userManager;
    private APIInteractor apiInteractor;
    private MenuView menuView;

    private boolean isInternetAvailable = false;

    public DashboardPresenterImpl(Context context, MenuView menuView) {
        this.context = context;
        this.menuView = menuView;
        userManager = new UserManager(context);
        apiInteractor = new APIInteractorImpl(context);
        isInternetAvailable = ApplicationUtils.checkInternet(context);
    }

    @Override
    public void initializePresenter() {
        menuView.initUI();
        menuView.initActionbar();
        menuView.setListener();
        menuView.setDashboard(DashboardUtils.getDashboardItems(userManager.getUser().getRole()));
    }

    @Override
    public void logOut() {
        menuView.showProgress(context.getString(R.string.logging_off_progress_message));
        apiInteractor.logOut(this);
    }

    @Override
    public void onLogOutSuccess() {
        menuView.hideProgress();
        userManager.removeUserSession();
        menuView.goToHomeScreen();
    }

    @Override
    public void onDashboardItemSelected(DashboardItem dashboardItem) {
        Intent intent = null;
        if (dashboardItem.getItemName().equalsIgnoreCase(DashboardUtils.SETTINGS)) {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        } else if (dashboardItem.getItemName().equalsIgnoreCase(DashboardUtils.EDIT_SLIDERS)) {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        } else if (dashboardItem.getItemName().equalsIgnoreCase(DashboardUtils.CLIENT_MANAGEMENT)) {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        } else if (dashboardItem.getItemName().equalsIgnoreCase(DashboardUtils.DISCOUNT_MANAGEMENT)) {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        } else if (dashboardItem.getItemName().equalsIgnoreCase(DashboardUtils.STOCK_MANAGEMENT)) {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        } else if (dashboardItem.getItemName().equalsIgnoreCase(DashboardUtils.ORDER_MANAGEMENT)) {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, dashboardItem.getItemName(), Toast.LENGTH_SHORT).show();
        }
        if (intent != null && isInternetAvailable) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Please connect with internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailed(String message) {
        menuView.hideProgress();
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }
}
