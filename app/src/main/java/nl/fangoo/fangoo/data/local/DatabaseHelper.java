package nl.fangoo.fangoo.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by wali on 5/16/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper sInstance;

    //  Database name
    static String DATABASE_NAME = "dnsDB";
    SQLiteDatabase db;

    //  Table For Cart
    public static final String USER_PRIVILEGES = "user_privileges";

    //Common field
    private static final String COLUMN_ID = "id";

    //User privileges table field
    public static final String COLUMN_PRIVILEGE = "privilege";

    //  Table create
//    private static final String
//            PRODUCT_TABLE_CREATE = "CREATE TABLE " + PRODUCT_TABLE + "(" + COLUMN_ID
//            + " INTEGER PRIMARY KEY autoincrement, " + COLUMN_PROD_ID
//            + " TEXT, " + COLUMN_PROD_CATEGORY
//            + " TEXT, " + COLUMN_PROD_SUB_CATEGORY
//            + " TEXT, " + COLUMN_PROD_NAME
//            + " TEXT, " + COLUMN_PROD_PCKG_SIZE
//            + " TEXT, " + COLUMN_PROD_DESCRIPTION
//            + " TEXT, " + COLUMN_PROD_MANUFACTURER
//            + " TEXT, " + COLUMN_PROD_SUPPLIER
//            + " TEXT, " + COLUMN_PROD_CODE
//            + " TEXT, " + COLUMN_PROD_PRICE
//            + " TEXT, " + COLUMN_PROD_DIRECT
//            + " TEXT, " + COLUMN_PROD_ORDER_DATE
//            + " TEXT, " + COLUMN_PROD_IS_PAID
//            + " TEXT, " + COLUMN_PROD_SELL_PRICE
//            + " TEXT, " + COLUMN_PROD_ORIG_PRICE
//            + " TEXT, " + COLUMN_PROD_THRESHOLD
//            + " TEXT, " + COLUMN_PROD_AMOUNT
//            + " TEXT, " + COLUMN_PROD_ORDERED_AMOUNT
//            + " TEXT)";

    private static final String
            USER_PRIVILEGES_CREATE = "CREATE TABLE " + USER_PRIVILEGES + "(" + COLUMN_ID
            + " INTEGER PRIMARY KEY autoincrement, " + COLUMN_PRIVILEGE
            + " TEXT)";

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USER_PRIVILEGES_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query1 = "DROP TABLE IF EXIST " + USER_PRIVILEGES;
        db.execSQL(query1);
        this.onCreate(db);

    }

    public void insertPrivileges(String privilege) {
//        deletePreviousData();
        deleteFromTable(USER_PRIVILEGES, "");
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] datas = new String[]{privilege};
        values = DatabaseHelperUtils.addStringContentValues(values, new String[]{COLUMN_PRIVILEGE}, datas);
        db.insert(USER_PRIVILEGES, null, values);
        db.close();
    }

    public ArrayList<String> getPrivileges() {
        ArrayList<String> privileges = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + USER_PRIVILEGES;

        db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String privilege = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PRIVILEGE));
                privileges.add(privilege);
            } while (cursor.moveToNext());
        }
        db.close();
        return privileges;
    }

//    public Item getItem(String[] select, String[] column_name, String[] value) {
//        Item item = new Item();
//        // Select All Query
//        String selectQuery = DatabaseHelperUtils.getSelectQuery(PRODUCT_TABLE, select, column_name, value);
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        if (cursor.moveToFirst()) {
//            item = DatabaseHelperUtils.itemSetter(cursor);
////            order.set(cursor.getString(cursor.getColumnIndex(COLUMN_BONUS_STOP)));
//        }
//        db.close();
//        return item;
//    }
//
//    public ArrayList<String> getSelectionFromItem(String select, String[] column_name, String[] value) {
//        ArrayList<String> strings = new ArrayList<>();
//        strings.add("None");
//        // Select All Query
//        String selectQuery = DatabaseHelperUtils.getSelectDistinctQuery(PRODUCT_TABLE, select, column_name, value);
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        if (cursor.moveToFirst()) {
//            do {
//                String selection = cursor.getString(cursor.getColumnIndex(select));
//                // Adding contact to list
//                strings.add(selection);
//            } while (cursor.moveToNext());
//        }
//        db.close();
//        return strings;
//    }
//
//    public void insertDiscount(Discount discount) {
////        deletePreviousData();
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        String[] datas = {discount.getId(), discount.getPercentage(), discount.getPercentage(), discount.getStart(), discount.getStop(), discount.getProduct_id()};
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_DISCOUNT, datas);
//        db.insert(DISCOUNT_TABLE, null, values);
////        Log.e("INFO", "One rowinserter " + count);
//        db.close();
//    }
//
//    public Discount getDiscount(String product_id) {
//        Discount discount = null;
//        // Select All Query
//        String selectQuery = DatabaseHelperUtils.getSelectQuery(DISCOUNT_TABLE, new String[]{}, new String[]{COLUMN_PROD_ID}, new String[]{product_id});
//
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            discount = DatabaseHelperUtils.discountSetter(cursor);
//        }
//
//        db.close();
//        // return contact list
//        return discount;
//    }
//
//
//    public void insertBonus(Bonus bonus) {
////        deletePreviousData();
//        String data[] = {bonus.getId(), bonus.getBonus_amount(), bonus.getProduct_amount(), bonus.getStart(), bonus.getStop(), bonus.getProduct_id()};
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_BONUS_INSERT, data);
//        db.insert(BONUS_TABLE, null, values);
////        Log.e("INFO", "One rowinserter " + count);
//        db.close();
//    }
//
//    public Bonus getBonus(String product_id) {
//        Bonus bonus = null;
//        // Select All Query
//        String selectQuery = DatabaseHelperUtils.getSelectQuery(BONUS_TABLE, new String[]{}, new String[]{COLUMN_PROD_ID}, new String[]{product_id});
//
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor.moveToFirst()) {
//            bonus = DatabaseHelperUtils.bonusSetter(cursor);
//        }
//        db.close();
//        return bonus;
//    }
//
//    public void insertBranches(Branch branch) {
////        deletePreviousData();
//        String data[] = {branch.getId(), branch.getName(), branch.getCode(), branch.getLocation()};
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_BRANCH_INSERT, data);
//        db.insert(BRANCH_TABLE, null, values);
////        Log.e("INFO", "One rowinserter " + count);
//        db.close();
//    }
//
//    public ArrayList<Branch> getBranches() {
//        ArrayList<Branch> branches = new ArrayList<Branch>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + BRANCH_TABLE;
//
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Branch branch = DatabaseHelperUtils.branchSetter(cursor);
//                // Adding contact to list
//                branches.add(branch);
//            } while (cursor.moveToNext());
//        }
//        db.close();
//        // return contact list
//        return branches;
//    }
//
//    public void insertCustomer(Customer customer) {
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        String[] datas = {customer.getId(), customer.getShop_name(), customer.getOwner(), customer.getAddress(),
//                customer.getType(), customer.getStatus(), customer.getProject(), customer.getContact_no(), customer.getDistrict(),
//                customer.getBranch_id(), customer.getBranch_name(), customer.getShop_code(), customer.getPrevious_credit()};
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_CUSTOMER_INSERT, datas);
//        db.insert(CUSTOMER_TABLE, null, values);
////        Log.e("INFO", "One rowinserter " + count);
//        db.close();
//    }
//
//    public ArrayList<Customer> getCustomers() {
//        ArrayList<Customer> customers = new ArrayList<Customer>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + CUSTOMER_TABLE;
//
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Customer customer = DatabaseHelperUtils.customerSetter(cursor);
//                // Adding contact to list
//                customers.add(customer);
//            } while (cursor.moveToNext());
//        }
//        db.close();
//        // return contact list
//        return customers;
//    }
//
//    public long insertOrder(Order order, String executive_id, String customer_id, String upload_status) {
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        String[] datas = {executive_id, order.getOrder_date(), order.getDeliver_date(), order.getIs_delivered(),
//                order.getOrder_code(), order.getIs_delivery_changed(), upload_status, customer_id, order.getMale(), order.getFemale()};
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_ORDER_INSERT, datas);
//        long id = db.insert(ORDER_TABLE, null, values);
////        Log.e("INFO", "One rowinserter " + count);
//        db.close();
//        return id;
//    }
//
//    public void updateOrder(Order order) {
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        String data[] = {order.getIs_delivered(), order.getOrder_code(), order.getIs_uploaded()};
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_ORDER_UPDATE, data);
//        db.update(ORDER_TABLE, values,
//                COLUMN_ORDER_ID + " = ?",
//                new String[]{order.getOrder_id()});
//        db.close();
//    }
//
//    public ArrayList<Order> getOrders(String[] column_names, String[] values) {
//        ArrayList<Order> orders = new ArrayList<Order>();
//        String selectQuery = DatabaseHelperUtils.getSelectQuery(ORDER_TABLE, new String[]{}, column_names, values);
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        if (cursor.moveToFirst()) {
//            do {
//                Order order = DatabaseHelperUtils.orderSetter(cursor);
//                // Adding contact to list
//                orders.add(order);
//            } while (cursor.moveToNext());
//        }
//        db.close();
//        // return contact list
//        return orders;
//    }
//
//    public void setUploadStatus(String status, String order_id) {
//        String data[] = {status};
//        String whereArgs = COLUMN_ORDER_ID + " = ?";
//        updateSingleValue(ORDER_TABLE, new String[]{COLUMN_ORDER_UPLOADED}, data, whereArgs, new String[]{order_id});
//    }
//
//    public void setOrderCode(String code, String order_id) {
//        String data[] = {code};
//        String whereArgs = COLUMN_ORDER_ID + " = ?";
//        updateSingleValue(ORDER_TABLE, new String[]{COLUMN_ORDER_CODE}, data, whereArgs, new String[]{order_id});
//    }
//
//    public void setDeliveryStatus(String status, String order_id) {
//        String data[] = {status};
//        String whereArgs = COLUMN_ORDER_ID + " = ?";
//        updateSingleValue(ORDER_TABLE, new String[]{COLUMN_ORDER_IS_DELIVERED}, data, whereArgs, new String[]{order_id});
//    }
//
//    public void setDeliveryChanged(String status, String order_id) {
//        String data[] = {status};
//        String whereArgs = COLUMN_ORDER_ID + " = ?";
//        updateSingleValue(ORDER_TABLE, new String[]{COLUMN_ORDER_IS_DELIVER_CHANGED}, data, whereArgs, new String[]{order_id});
//    }
//
//    public void setDeliveryStatus(String status, String order_id, String product_id) {
//        String data[] = {status};
//        String whereArgs = COLUMN_OP_ORDER_ID + " = ? AND " + COLUMN_OP_PROD_ID + " = ?";
//        updateSingleValue(OP_TABLE, new String[]{COLUMN_OP_IS_DELIVERED}, data, whereArgs, new String[]{order_id, product_id});
//    }
//
//    public void setPayment(String paid, String order_id, String product_id) {
//        String data[] = {paid};
//        String whereArgs = COLUMN_OP_ORDER_ID + " = ? AND " + COLUMN_OP_PROD_ID + " = ?";
//        updateSingleValue(OP_TABLE, new String[]{COLUMN_OP_PAID}, data, whereArgs, new String[]{order_id, product_id});
//    }
//
//    public void updateSingleValue(String table_name, String[] column_names, String[] data, String whereArgs, String[] whereValue) {
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values = DatabaseHelperUtils.addStringContentValues(values, column_names, data);
//        db.update(table_name, values,
//                whereArgs,
//                whereValue);
//        db.close();
//    }
//
//    public void deleteOrder(String order_id) {
//        String whereClause = " WHERE " + COLUMN_ORDER_ID + " ='" + order_id + "';";
//        deleteFromTable(ORDER_TABLE, whereClause);
//    }
//
//    public void insertOrderedProducts(String order_id, String product_id, Item item) {
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        String[] datas = {order_id, product_id, item.getAmount(), item.getPrice(), "0", item.getIs_direct(), "0"};
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_OP_INSERT, datas);
//        long id = db.insert(OP_TABLE, null, values);
//        Log.e("INSERT", "LAST ID " + id);
//        db.close();
//    }
//
//    public void updateOrderedProducts(Item item, String order_id) {
//        db = this.getWritableDatabase();
//        String id = TextUtils.isEmpty(item.getId()) ? item.getProduct_id() : item.getId();
//        String data[] = {item.getIs_delivered(), item.getIs_direct(), item.getPaid()};
//        ContentValues values = new ContentValues();
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_OP_UPDATE, data);
//        db.update(OP_TABLE, values,
//                COLUMN_OP_ORDER_ID + " = ? AND " + COLUMN_OP_PROD_ID + " = ?",
//                new String[]{order_id, id});
//        db.close();
//    }
//
//    public ArrayList<Item> getOrderedProducts(String[] column_names, String[] values) {
//        ArrayList<Item> items = new ArrayList<Item>();
//        // Select All Query
//        String selectQuery = DatabaseHelperUtils.getSelectQuery(OP_TABLE, new String[]{}, column_names, values);
//
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Item item = new Item();
//                item.setProduct_id(cursor.getString(cursor.getColumnIndex(COLUMN_OP_PROD_ID)));
//                item.setId(cursor.getString(cursor.getColumnIndex(COLUMN_OP_PROD_ID)));
//                item.setAmount(cursor.getString(cursor.getColumnIndex(COLUMN_OP_AMOUNT)));
//                item.setPrice(cursor.getString(cursor.getColumnIndex(COLUMN_OP_PRICE)));
//                item.setIs_delivered(cursor.getString(cursor.getColumnIndex(COLUMN_OP_IS_DELIVERED)));
//                item.setIs_direct(cursor.getString(cursor.getColumnIndex(COLUMN_OP_IS_DIRECT)));
//                item.setPaid(cursor.getString(cursor.getColumnIndex(COLUMN_OP_PAID)));
//                // Adding contact to list
//                items.add(item);
//            } while (cursor.moveToNext());
//        }
//        db.close();
//        // return contact list
//        return items;
//    }
//
//    public int countRow(String order_code) {
//        String query = "SELECT * FROM " + ORDER_TABLE + " WHERE " + COLUMN_ORDER_CODE + " = '" + order_code + "';";
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(query, null);
//        int cnt = cursor.getCount();
//        cursor.close();
//        db.close();
//        return cnt;
//    }
//
//    public void insertItemInInventory(Item item) {
////        deletePreviousData();
//        db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        String[] datas;
//        datas = new String[]{item.getId(), item.getCategory(), item.getSub_category(), item.getName(), item.getPackage_size(), item.getDescription(), item.getManufacturer(),
//                item.getSupplier(), item.getCode(), item.getSelling_price(), item.getOriginal_price(), item.getThreshold(), item.getAmount(), item.getOrdered_amount()};
//        values = DatabaseHelperUtils.addStringContentValues(values, COLUMN_INVENTORY, datas);
//        db.insert(INVENTORY_TABLE, null, values);
////        Log.e("INFO", "One rowinserter " + count);
//        db.close();
//    }
//
//    public ArrayList<Item> getInventoryItems() {
//        ArrayList<Item> items = new ArrayList<Item>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + INVENTORY_TABLE;
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Item item = DatabaseHelperUtils.inventoryItemSetter(cursor);
//                // Adding contact to list
//                items.add(item);
//            } while (cursor.moveToNext());
//        }
//        db.close();
//        // return contact list
//        return items;
//    }


    public void clearDatabase() {
        deleteFromTable(USER_PRIVILEGES, "");
    }

    public void deleteFromTable(String table_name, String whereClause) {
        db = getWritableDatabase();
        db.execSQL("delete from " + table_name + whereClause);
        db.close();
    }

}
