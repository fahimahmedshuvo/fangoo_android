
package nl.fangoo.fangoo.data.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import nl.fangoo.fangoo.model.User;

public class LoginResponse extends CommonResponse{

    @SerializedName("user_info")
    @Expose
    private User user;
    /**
     * 
     * @return
     *     The userInfo
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user_info
     */
    public void setUser(User user) {
        this.user = user;
    }
}
