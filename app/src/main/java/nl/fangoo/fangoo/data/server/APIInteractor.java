package nl.fangoo.fangoo.data.server;

import java.util.ArrayList;

import nl.fangoo.fangoo.model.User;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public interface APIInteractor {
    void userLogin(LoginListener loginListener, String email, String password);
    void logOut(LogOutListener logOutListener);

    interface LoginListener extends FailedListener{
        void onLoginSuccess(User user);
    }

    interface LogOutListener extends FailedListener{
        void onLogOutSuccess();
    }

    interface FailedListener {
        void onFailed(String message);
    }
}
