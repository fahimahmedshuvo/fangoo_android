package nl.fangoo.fangoo.data.server;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.data.responses.CommonResponse;
import nl.fangoo.fangoo.data.responses.LoginResponse;
import nl.fangoo.fangoo.model.User;
import nl.fangoo.fangoo.utils.ApplicationUtils;
import nl.fangoo.fangoo.utils.UserManager;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class APIInteractorImpl implements APIInteractor {
    private Retrofit retrofit;
    private APIServiceInterface apiServiceInterface;
    private Context context;
    private UserManager userManager;
    private String token;

    private static final String BASE_URL = "http://fangoo.montorlabs.com/api/";

    public APIInteractorImpl(Context context) {
        this.context = context;
        userManager = new UserManager(context);
        initAPIService(BASE_URL);
    }

    private void initAPIService(String url) {

        OkHttpClient client = ApplicationUtils.getClient(context);
        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiServiceInterface = retrofit.create(APIServiceInterface.class);
    }

    @Override
    public void userLogin(final LoginListener loginListener, String email, String password) {
        Call<LoginResponse> call = apiServiceInterface.loginUser(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body().getSuccess() == 1) {
                    loginListener.onLoginSuccess(response.body().getUser());
                } else {
                    loginListener.onFailed(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginListener.onFailed(context.getString(R.string.conn_failed));
            }
        });
    }


    @Override
    public void logOut(final LogOutListener logOutListener) {
        User user = userManager.getUser();
        String token = user.getToken();
        Call<CommonResponse> call = apiServiceInterface.logOut(userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.body().getSuccess() == 1) {
                    logOutListener.onLogOutSuccess();
                } else {
                    logOutListener.onFailed(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                logOutListener.onFailed(t.getMessage());
            }
        });
    }
}
