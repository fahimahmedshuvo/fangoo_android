package nl.fangoo.fangoo.data.server;

import nl.fangoo.fangoo.data.responses.CommonResponse;
import nl.fangoo.fangoo.data.responses.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIServiceInterface {
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> loginUser(@Field("email") String identity, @Field("password") String password);

    @GET("logout")
    Call<CommonResponse> logOut(@Query("token") String token);
}
