package nl.fangoo.fangoo.utils;

import android.text.TextUtils;
import android.widget.EditText;

import java.util.regex.Pattern;

import nl.fangoo.fangoo.customcontrol.FangooTextInputEditText;
import nl.fangoo.fangoo.customcontrol.FangooTextInputLayout;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class Validator {
    public static boolean checkValidity(EditText editText, String errorText, String textType) {
        String text = editText.getText().toString();
        if (TextUtils.isEmpty(text)) {
            editText.setError(errorText);
            return false;
        } else {
            if (textType.equalsIgnoreCase("email")) {
                if (text.matches(StaticData.EMAIL_REGEX)) {
                    return true;
                } else {
                    editText.setError("Not a valid email address");
                    return false;
                }
            }else if(textType.equalsIgnoreCase("password")){
                if(text.length()<6){
                    editText.setError("Password must contain 6 alphabets");
                    return false;
                }else{
                    return true;
                }
            }else{
                return true;
            }
        }
    }

    public static boolean checkValidity(FangooTextInputLayout dnsTextInputLayout, String input, String errorText, String textType){
        if(TextUtils.isEmpty(input)){
            dnsTextInputLayout.setError(errorText);
            return false;
        }else{
            if(textType.equalsIgnoreCase("number")){
                if(isInputTypeNumber(input)){
                    dnsTextInputLayout.setError(null);
                    return true;
                }else{
                    dnsTextInputLayout.setError("Invalid input");
                    return false;
                }
            }else{
                dnsTextInputLayout.setError(null);
                return true;
            }
        }
    }

    public static boolean isValidLicensePlate(FangooTextInputLayout dnsTextInputLayout, FangooTextInputEditText editText, String input, String errorText){
        final Pattern licensePlatePattern
                = Pattern.compile("(([a-zA-Z]{3}[0-9]{3})|(\\w{2}-\\w{2}-\\w{2})|([0-9]{2}-[a-zA-Z]{3}-[0-9]{1})|([0-9]{1}-[a-zA-Z]{3}-[0-9]{2})|([a-zA-Z]{1}-[0-9]{3}-[a-zA-Z]{2}))\n");

        if(TextUtils.isEmpty(input)){
            editText.setError(errorText);
            dnsTextInputLayout.setError(errorText);
            return false;
        }else{
            if(licensePlatePattern.matcher(input).matches()){
                return true;
            }else{
                editText.setError(errorText);
                dnsTextInputLayout.setError(errorText);
                return false;
            }
        }
    }

    public static boolean isInputTypeNumber(String text){
        try {
            int number = Integer.parseInt(text);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }
}
