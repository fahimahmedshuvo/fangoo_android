package nl.fangoo.fangoo.utils;

import java.util.ArrayList;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.base.FangooApplication;
import nl.fangoo.fangoo.model.DashboardItem;

/**
 * Created by Fahim Ahmed on 14-12-16.
 */

public class DashboardUtils {
    //Privileges 1st level
    public static final String EDIT_SLIDERS = FangooApplication.getApplication().getResources().getString(R.string.dashboard_item_edit_sliders);
    public static final String CLIENT_MANAGEMENT = FangooApplication.getApplication().getResources().getString(R.string.dashboard_item_client_management);
    public static final String DISCOUNT_MANAGEMENT = FangooApplication.getApplication().getResources().getString(R.string.dashboard_item_discount_management);
    public static final String STOCK_MANAGEMENT = FangooApplication.getApplication().getResources().getString(R.string.dashboard_item_stock_management);
    public static final String ORDER_MANAGEMENT = FangooApplication.getApplication().getResources().getString(R.string.dashboard_item_order_management);
    public static final String SETTINGS = FangooApplication.getApplication().getResources().getString(R.string.dashboard_item_settings);

    private static final String[] ADMIN_PERMISSION_NAMES = {EDIT_SLIDERS,CLIENT_MANAGEMENT, DISCOUNT_MANAGEMENT, STOCK_MANAGEMENT, ORDER_MANAGEMENT,SETTINGS};

    private static final String[] RESELLER_PERMISSION_NAMES = {EDIT_SLIDERS,CLIENT_MANAGEMENT, DISCOUNT_MANAGEMENT, STOCK_MANAGEMENT, ORDER_MANAGEMENT};

    public static ArrayList<DashboardItem> getDashboardItems(String role) {
        if (role.equalsIgnoreCase("admin")) {
            return adminDashboard();
        } else {
            return resellerDashboard();
        }
    }

    private static ArrayList<DashboardItem> adminDashboard() {
        ArrayList<DashboardItem> dashboardItems = new ArrayList<>();
        for(int i = 0; i< ADMIN_PERMISSION_NAMES.length; i++){
            DashboardItem dashboardItem = new DashboardItem(ADMIN_PERMISSION_NAMES[i]);
            dashboardItems.add(dashboardItem);
        }
        return dashboardItems;
    }

    private static ArrayList<DashboardItem> resellerDashboard() {
        ArrayList<DashboardItem> dashboardItems = new ArrayList<>();
        for(int i = 0; i< RESELLER_PERMISSION_NAMES.length; i++){
            DashboardItem dashboardItem = new DashboardItem(RESELLER_PERMISSION_NAMES[i]);
            dashboardItems.add(dashboardItem);
        }
        return dashboardItems;
    }

}
