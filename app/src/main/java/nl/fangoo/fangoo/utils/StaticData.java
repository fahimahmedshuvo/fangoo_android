package nl.fangoo.fangoo.utils;

/**
 * Created by George on 27-July-2016.
 */
public class StaticData {

    /**
     * These here is the must change static data for each application
     **/

    public static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    //Session
    public static final String PREFERENCE_NAME = "app_preference";
    public static final String LOGIN_STATE = "login_state";

    //User
    public static final String USER = "USER";
    public static final String USER_LIST = "user_list";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "email";
    public static final String USER_ROLE = "role";
    public static final String USER_PRIVILEGE = "privilege";
    public static final String USER_TOKEN = "token";
    public static final String USER_DEFAULT_PRIVILEGE_LIST = "default_privilege_list";
    public static final String CATEGORIES = "categories";

    public static final String IS_DELIVERY = "is_delivery";

    //Firebase
    public static final String FCM_TOKEN = "fcm_token";
    public static final String NOTIFICATION_MESSAGE = "message";
    public static final String TITLE = "title";
}
