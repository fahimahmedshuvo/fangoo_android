package nl.fangoo.fangoo.utils;

import android.content.Context;

import java.util.ArrayList;

import nl.fangoo.fangoo.base.FangooApplication;
import nl.fangoo.fangoo.model.User;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class UserManager {
    private TinyDB tinyDB;

    public UserManager(Context context) {
        tinyDB = FangooApplication.getApplication().getTinyDB();
    }

    public boolean isLoggedIn() {
        return tinyDB.getBoolean(StaticData.LOGIN_STATE);
    }

    public User getUser() {
        return (User) tinyDB.getObject(StaticData.USER, User.class);
    }

    public void saveUserSession(User user) {
        tinyDB.putBoolean(StaticData.LOGIN_STATE, true);
        tinyDB.putObject(StaticData.USER, user);
    }

    public void removeUserSession() {
        tinyDB.putBoolean(StaticData.LOGIN_STATE, false);
        tinyDB.remove(StaticData.USER);
    }

    public String getUsername(String id) {
        ArrayList<User> users = tinyDB.getUserList(StaticData.USER_LIST, User.class);
        if (users != null) {
            for (User user :
                    users) {
                if (user.getId().equalsIgnoreCase(id)) {
                    return user.getName();
                }
            }
        }
        return "";
    }

    public ArrayList<User> getAllUsers(String type){
        ArrayList<User> users = tinyDB.getUserList(StaticData.USER_LIST, User.class);
        if(type==null){
            return users;
        }else{
            ArrayList<User> typedUsers = new ArrayList<>();
            for (User user :
                    users) {
                if (user.getRole().equalsIgnoreCase(type)) {
                    typedUsers.add(user);
                }
            }
            return typedUsers;
        }
    }

    public String getDeviceTOken(){
        return tinyDB.getString(StaticData.FCM_TOKEN);
    }
}
