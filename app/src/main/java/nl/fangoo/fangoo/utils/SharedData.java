package nl.fangoo.fangoo.utils;

import java.util.ArrayList;
import java.util.List;

import nl.fangoo.fangoo.model.User;

/**
 * Created by Fahim Ahmed on 12-11-2016
 */

public final class SharedData {
    private static SharedData instance = new SharedData();

    private SharedData() {
    }

    /**
     * @return
     */
    public static SharedData getInstance() {
        return instance;
    }

    /**
     * @param instance
     */
    public static void setInstance(SharedData instance) {
        SharedData.instance = instance;
    }

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
