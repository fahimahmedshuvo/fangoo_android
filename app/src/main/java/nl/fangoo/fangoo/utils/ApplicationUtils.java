package nl.fangoo.fangoo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.os.IBinder;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import nl.fangoo.fangoo.R;
import nl.fangoo.fangoo.customcontrol.FangooTextInputEditText;
import nl.fangoo.fangoo.customcontrol.FangooTextInputLayout;
import nl.fangoo.fangoo.customcontrol.InstantAutoComplete;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class ApplicationUtils {
    public static void setTextColor(TextView tvText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvText.setTextColor(context.getColor(resId));
        } else {
            tvText.setTextColor(context.getResources().getColor(resId));
        }
    }

    public static void setHintTextColor(TextView tvText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvText.setHintTextColor(context.getColor(resId));
        } else {
            tvText.setHintTextColor(context.getResources().getColor(resId));
        }
    }

    public static void setHintTextColor(TextInputEditText tvText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvText.setHintTextColor(context.getColor(resId));
        } else {
            tvText.setHintTextColor(context.getResources().getColor(resId));
        }
    }

    public static Typeface getTypeface(int style, Context context) {
        switch (style) {
            case Typeface.BOLD:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
            case Typeface.ITALIC:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Italic.ttf");
            case Typeface.NORMAL:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
            default:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");

        }
    }

    public static View.OnFocusChangeListener getOnFocusChangedListener(final TextInputLayout textInputLayout, final String error_text) {
        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v instanceof FangooTextInputEditText || v instanceof InstantAutoComplete) {
                    if (hasFocus) {
                        if (textInputLayout.getError() != null) {
                            textInputLayout.setError(null);
                        } else {
                            return;
                        }
                    } else {
                        String input = "";
                        if (v instanceof FangooTextInputEditText) {
                            FangooTextInputEditText textInputEditText = (FangooTextInputEditText) v;
                            input = textInputEditText.getText().toString();
                        } else {
                            input = "";
                        }
                        if (TextUtils.isEmpty(input)) {
                            textInputLayout.setError(error_text);
                        } else {
                            return;
                        }
                    }
                }
            }
        };
        return onFocusChangeListener;
    }

    public static Animation loadZoomInOutAnimation(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.zoom_in_out);
    }

//    private static Cache get_cache(Context context) {
//        File httpCacheDir;
//        Cache cache = null;
//        try {
//            httpCacheDir = new File(context.getCacheDir(), "http");
//            httpCacheDir.setReadable(true);
//            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
//            HttpResponseCache.install(httpCacheDir, httpCacheSize);
//            cache = new Cache(httpCacheDir, httpCacheSize);
//            Log.i("HTTP Caching", "HTTP response cache installation success");
//        } catch (IOException e) {
//            Log.i("HTTP Caching", "HTTP response cache installation failed:" + e);
//        }
//        return cache;
//    }
//
//    public static OkHttpClient get_client(final Context context) {
//        OkHttpClient client = new OkHttpClient
//                .Builder()
//                .cache(get_cache(context)) // 10 MB
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public okhttp3.Response intercept(Chain chain) throws IOException {
//                        Request request = chain.request();
//                        if (ApplicationUtils.checkInternet(context)) {
//                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
//                        } else {
//                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
//                        }
//                        return chain.proceed(request);
//                    }
//                })
//                .build();
//        return client;
//    }

    public static OkHttpClient getClient(final Context context) {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient
                .Builder()
                .cache(getCache(context)) // 10 MB
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        if (ApplicationUtils.checkInternet(context)) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                        }
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(httpLoggingInterceptor)
                .build();
        return client;
    }

    private static Cache getCache(Context context) {
        File httpCacheDir;
        Cache cache = null;
        try {
            httpCacheDir = new File(context.getCacheDir(), "http");
            httpCacheDir.setReadable(true);
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
            cache = new Cache(httpCacheDir, httpCacheSize);
            Log.i("HTTP Caching", "HTTP response cache installation success");
        } catch (IOException e) {
            Log.i("HTTP Caching", "HTTP response cache installation failed:" + e);
        }
        return cache;
    }

    public static boolean checkInternet(Context context) {
        ConnectivityManager check = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = check.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static TextWatcher addTextWatcher(final FangooTextInputLayout dnsTextInputLayout) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dnsTextInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        return textWatcher;
    }

    /**
     * Hides the keyboard.
     *
     * @param activity the Activity
     */
    public static void hideKeyboard(final Activity activity) {
        try {
            if (activity != null) {
                final InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                final View view = activity.getCurrentFocus();
                if (view != null) {
                    final IBinder binder = view.getWindowToken();
                    imm.hideSoftInputFromWindow(binder, 0);
                    imm.showSoftInputFromInputMethod(binder, 0);
                }
            }
        } catch (final Exception e) {
            Log.d(ApplicationUtils.class.getSimpleName(), "Exception to hide keyboard", e);
        }
    }

    public static void showAlertDialog(String message, Context context, String positiveText, String negativeText,
                                       DialogInterface.OnClickListener positiveCallback, DialogInterface.OnClickListener negativeCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);

        builder.setPositiveButton(positiveText, positiveCallback);
        if(!TextUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText, negativeCallback);
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
