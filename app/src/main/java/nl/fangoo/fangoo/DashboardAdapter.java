package nl.fangoo.fangoo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nl.fangoo.fangoo.model.DashboardItem;
import nl.fangoo.fangoo.presenter.DashboardPresenter;

/**
 * Created by Fahim Ahmed on 14-12-16.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardViewHolder> {

    private ArrayList<DashboardItem> dashboardItems;
    private Context context;
    private DashboardPresenter dashboardPresenter;

    public DashboardAdapter(ArrayList<DashboardItem> dashboardItems, Context context, DashboardPresenter dashboardPresenter) {
        this.dashboardItems = dashboardItems;
        this.context = context;
        this.dashboardPresenter = dashboardPresenter;
    }

    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_cell_dashboard, parent, false);
        return new DashboardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DashboardViewHolder holder, final int position) {
        final DashboardItem dashboardItem = dashboardItems.get(position);
        holder.tvTitle.setText(dashboardItem.getItemName());
        holder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dashboardPresenter.onDashboardItemSelected(dashboardItem);
            }
        });
    }


    @Override
    public int getItemCount() {
        return dashboardItems.size();
    }

    public static class DashboardViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected ImageView ivThumb;
        protected LinearLayout llContainer;

        public DashboardViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            llContainer = (LinearLayout) itemView.findViewById(R.id.ll_container);
        }
    }
}

