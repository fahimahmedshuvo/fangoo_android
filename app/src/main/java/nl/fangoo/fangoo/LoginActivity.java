package nl.fangoo.fangoo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import nl.fangoo.fangoo.customcontrol.FangooButton;
import nl.fangoo.fangoo.customcontrol.FangooEditText;
import nl.fangoo.fangoo.customcontrol.FangooTextView;
import nl.fangoo.fangoo.presenter.LoginPresenter;
import nl.fangoo.fangoo.presenter.LoginPresenterImpl;
import nl.fangoo.fangoo.utils.Initializer;
import nl.fangoo.fangoo.utils.Validator;
import nl.fangoo.fangoo.view.LoginView;

/**
 * Created by Fahim Ahmed on 11-12-16.
 */

public class LoginActivity extends AppCompatActivity implements LoginView {

    private Context context;
    private LoginPresenter loginPresenter;
    private ProgressBar pbLogin;
    private FangooEditText etUserName, etPassword;
    private FangooButton btnLogin;
    private Toolbar toolbar;
    private FangooTextView tvAppbarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        loginPresenter = new LoginPresenterImpl(context, this);
        loginPresenter.initializePresenter();
    }

    @Override
    public void showProgress(String message) {
        pbLogin.setVisibility(View.VISIBLE);
        etPassword.setEnabled(false);
        etUserName.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        pbLogin.setVisibility(View.GONE);
        etPassword.setEnabled(true);
        etUserName.setEnabled(true);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initUI() {
        etUserName = Initializer.initialize(etUserName, R.id.etUsername, this);
        etPassword = Initializer.initialize(etPassword, R.id.etPassword, this);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        btnLogin = Initializer.initialize(btnLogin, R.id.btn_login, this);
        toolbar = Initializer.initialize(toolbar, R.id.toolbar, this);
        pbLogin = Initializer.initialize(pbLogin, R.id.pbLogin, this);
        tvAppbarTitle = Initializer.initialize(R.id.tv_appbar_title,toolbar);
    }

    @Override
    public void initActionbar() {
        setSupportActionBar(toolbar);
        tvAppbarTitle.setText(getString(R.string.appbar_text_login));
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public void setListener() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        etPassword.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            login();
                            return true;
                        }
                        return false;
                    }
                });
    }

    @Override
    public void goToDashboard() {
        Intent intent = new Intent(context, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    private void login(){
        boolean isUserNameValid = Validator.checkValidity(etUserName, getString(R.string.email_validation_error_message), "email");
        boolean isPasswordValid = Validator.checkValidity(etPassword, getString(R.string.password_validation_error_message), "password");
        if (isUserNameValid && isPasswordValid) {
            loginPresenter.loginUser(etUserName.getText().toString(), etPassword.getText().toString());
        }
    }
}
